from chatbot.settings.settings import *

DEBUG = True
CORS_ORIGIN_ALLOW_ALL = True

"""
Test Database connection
connect to your local database
"""

MONGO_DATABASE_NAME = 'makoto'
MONGO_HOST = '127.0.0.1'
MONGO_PORT = 27017
MONGO_CLIENT = MongoClient(MONGO_HOST,MONGO_PORT)
MONGO_DATABASE = MONGO_CLIENT[MONGO_DATABASE_NAME]
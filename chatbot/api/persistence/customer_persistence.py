from chatbot.api.process.message import Message
from pymongo import MongoClient
from chatbot.api.services.campaign_service import CampaignService
from chatbot.api.services.employee_service import EmployeeService
from django.conf import settings
"""
    Import bson to returns the query result.
"""


class CustomerPersistence:

    def __init__(self):

        self.message = Message()
        self.campaing = CampaignService()
        self.employees = EmployeeService()
        self.customer = None
        self.customer_token = ""
        self.chosen_product = ""
        MONGO_DATABASE_NAME = 'makoto'
        MONGO_HOST = '127.0.0.1'
        MONGO_PORT = 27017
        MONGO_CLIENT = MongoClient(MONGO_HOST, MONGO_PORT)
        MONGO_DATABASE = MONGO_CLIENT[MONGO_DATABASE_NAME]
        self.collection = MONGO_DATABASE['prueba']
        self.ofertas = MONGO_DATABASE['ofertas']
        self.solicitudes = MONGO_DATABASE['solicitudes']
        #self.collection = settings.MONGO_DATABASE['prueba']

    def get_customer_by_phone(self,phone):
        customer_response = {}
        self.customer = self.collection.find_one({
            "Telefono": phone
        })
        self.message.process_customer_offer(self.customer)
        message = self.message.message
        customer_response["Mensaje"] = message
        customer_response["Nombres"] = self.customer["Nombres"]
        return customer_response

    def process_customer_answer(self, customer_answer):
        message = {}
        if(customer_answer["state"] == 1):
            if("response" in customer_answer):
                customer_answer = customer_answer["response"]
                self.chosen_product=self._customer_offer(customer_answer)
                self.message.processing_customer_messages(self.customer,self.chosen_product)
                self.customer_token = self.message.token
                message["Mensaje"] = self.message.message
                message["Token"] = self.customer_token
        elif(customer_answer["state"] == 2):
            if("token" in customer_answer):
                customer_answer = customer_answer["token"]
                self.testing_token_response(customer_answer,message)
                message["Mensaje"] = self.message.message
        elif(customer_answer["state"] == 3):
            if ("response" in customer_answer):
                customer_answer = customer_answer["response"]
                if("no" not in customer_answer.lower()):
                    information,estado = self.information_customer(customer_answer.lower())
                    if(estado == "C"):
                        self.message.message = []
                        self.message.message.append("Nuestras campañas son: ")
                        self.message.message+=information
                    elif(estado == "A"):
                        self.message.message = []
                        self.message.message.append("Nuestros asesores son: ")
                        self.message.message+=information
                else:
                    self.message.message=[]
                    self.message.message.append("Muchas gracias por tu tiempo, "+
                                                "que tengas un buen día")
            message["Mensaje"] = self.message.message
            message["Token"] = ""
        elif(customer_answer["state"] == 4):
            self.message.message.append("Muchas gracias, esperamos haberte ayudado")
            message["Mensaje"] = self.message.message
            message["Token"] = ""
        return message

    def information_customer(self,customer_answer):
        information = []
        estado = ""
        if("campañas" in customer_answer.lower() or "campaña" in customer_answer.lower()):
            for campaing in self.campaing.find_campaigns_by_status("A"):
                information.append("Nombre campaña: "+campaing["name"])
                information.append("Fecha de finalización: "+campaing["finish_date"])
                information.append("Descripción: "+campaing["description"])
                information.append("Condiciones: "+campaing["conditions"])
            estado = "C"
        if("asesores" in customer_answer.lower() or "asesoria" in customer_answer.lower()):
            for employee in self.employees.find_employee_by_helpers():
                information.append("Nombre del empleado: "+employee["name"])
                information.append("Correo del empleado: "+employee["email"])
                information.append("Rol del empleado: "+employee["rol"])
            estado = "A"
        return information,estado

    def testing_token_response(self, customer_send_token,message):
        if(customer_send_token == self.customer_token):
            self.message.contract_product_bank(self.chosen_product)
            message["Token"] = ""
        else:
            self.message.message = []
            self.message.message.append("El código digitado es incorrecto, por favor vuelva a intentarlo")
            message["Token"] = self.customer_token

    def  _customer_offer(self,product_id):
        customer_product = ""
        productos = self.ofertas.find_one({"valor":self.customer["Estrellas"]})["productos"]
        for producto in productos:
            if(product_id in producto["id"]):
                customer_product = producto["producto"]
        return customer_product

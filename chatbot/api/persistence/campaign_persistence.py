from django.conf import settings


class CampaignPersistence:

    def __init__(self):
        """
            Constructor
        """
        self.collection = settings.MONGO_DATABASE['campaign']

    def find_campaign_by_name(self, name):
        """
            Find a campaign by name.
        :return:
        """
        all_campaigns = self.collection.find({})
        res = []
        for i in all_campaigns:
            if name.upper() in i["name"].upper():
                i["_id"] = str(i["_id"])
                res.append(i)
        return res

    def find_campaign_by_description(self, description):
        """
            Find a campaign by description.
        :return:
        """
        all_campaigns = self.collection.find({})
        res = []
        description = description.split(" ")
        for i in all_campaigns:
            for j in description:
                if j.lower() in i["description"].lower():
                    i["_id"] = str(i["_id"])
                    res.append(i)
        return res

    def find_all_campaigns(self):
        """
            Find all the campaigns
        :return:
        """
        res = []
        all_campaigns = self.collection.find({})
        for i in all_campaigns:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

    def find_all_campaigns_active(self):
        """
            Find all the campaigns which are active
        :return:
        """
        res = []
        all_campaigns = self.collection.find({"state": "A"})
        for i in all_campaigns:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

    def find_all_campaigns_deprecated(self):
        """
            Find all the campaigns which are not available.
        :return:
        """
        res = []
        all_campaigns = self.collection.find({"state": "N"})
        for i in all_campaigns:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res


import pandas as pd
from pymongo import MongoClient
from chatbot.api.persistence.customer_persistence import CustomerPersistence

def mongo_connection():
    MONGO_DATABASE_NAME = 'makoto'
    MONGO_HOST = '127.0.0.1'
    MONGO_PORT = 27017
    MONGO_CLIENT = MongoClient(MONGO_HOST, MONGO_PORT)
    MONGO_DATABASE = MONGO_CLIENT[MONGO_DATABASE_NAME]

    collection = MONGO_DATABASE['prueba']
    ofertas = MONGO_DATABASE['ofertas']

    offer6 = {"valor":"6","productos":[{"id":"1","producto":"CDT"},
                                       {"id":"2","producto":"Cuenta de ahorros"},
                                       {"id":"3","producto":"Fondo de inversion"},
                                       {"id":"4","producto":"Tarjeta de credito"},
                                       {"id":"5","producto":"Prestamo hipotecario"},
                                       {"id":"6","producto":"Libre inversion"},
                                       {"id": "7","producto": "NO"}]}
    offer5 = {"valor":"5","productos":[{"id":"1","producto":"CDT"},
                                       {"id":"2","producto":"Cuenta de ahorros"},
                                       {"id":"3","producto":"Fondo de inversion"},
                                       {"id": "4", "producto": "NO"}]}
    offer4 = {"valor":"4","productos":[{"id":"1","producto":"Reestructuracion"},
                                       {"id":"2", "producto": "NO"}]}

    offer3 = {"valor": "3", "productos": [{"id": "1", "producto": "Sobregiro"},
                                          {"id": "2", "producto": "NO"}]}

    offer2 = {"valor": "2", "productos": [{"id": "1", "producto": "Renovacion"},
                                          {"id": "2", "producto": "NO"}]}

    ofertas.insert(offer2)
    ofertas.insert(offer3)
    ofertas.insert(offer4)
    ofertas.insert(offer5)
    ofertas.insert(offer6)

    df = pd.read_parquet('C:\\Users\\Danie\\makoto-chat-bot-bbva\\hackathon-chat\\chatbot\\resource')
    numero_telefono = {}
    for index, row in df.iterrows():
        customer = {}
        for column in df:
            try:
                customer[column] = str(row[column].strip())
            except:
                customer[column] = str(row[column])
        if (row["Telefono"] not in numero_telefono):
            collection.insert(customer)
            numero_telefono[row["Telefono"]] = 1



def test_message_methods():
    customer_persistence = CustomerPersistence()
    men1 = customer_persistence.get_customer_by_phone("3089940926")
    print(men1)
    men2 = customer_persistence.process_customer_answer({"response":"3"})
    print(men2,customer_persistence.chosen_product)
    #men3 = customer_persistence.process_customer_answer({"token":"c4d00e6"})
    #print(men3)

def parquetBrayito():
    MONGO_DATABASE_NAME = 'makoto'
    MONGO_HOST = '127.0.0.1'
    MONGO_PORT = 27017
    MONGO_CLIENT = MongoClient(MONGO_HOST, MONGO_PORT)
    MONGO_DATABASE = MONGO_CLIENT[MONGO_DATABASE_NAME]
    ofertas = MONGO_DATABASE['ofertas']
    productos = ofertas.find_one({"valor":"5", "productos.id":"4"})





def main():
    #parquetBrayito()
    #test_message_methods()
    mongo_connection()



main()
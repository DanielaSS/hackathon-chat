from django.conf import settings


class EmployeePersistence:

    def __init__(self):
        """
            Constructor
        """
        self.collection = settings.MONGO_DATABASE['employee']

    def find_employee_by_name(self, name):
        """
            Find a employee by name.
        :return:
        """
        all_employees = self.collection.find({})
        res = []
        for i in all_employees:
            if name.upper() in i["name"].upper():
                i["_id"] = str(i["_id"])
                res.append(i)
        return res

    def find_employee_by_area(self, name):
        """
            Find a employee by name.
        :return:
        """
        all_employees = self.collection.find({})
        res = []
        for i in all_employees:
            if name.upper() in i["area"].upper():
                i["_id"] = str(i["_id"])
                res.append(i)
        return res

    def find_employee_by_helpers(self):
        """
            Find customer helpers
        :return:
        """
        return self.collection.find({"rol": "ATENCION CLIENTE"}).limit(5)

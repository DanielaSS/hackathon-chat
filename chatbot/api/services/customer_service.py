from chatbot.api.persistence.customer_persistence import CustomerPersistence

class CustomerService:

    def __init__(self):
        self.customer_persistence = CustomerPersistence()

    def get_customer_by_phone(self,phone):
        return self.customer_persistence.get_customer_by_phone(phone)

    def process_customer_answer(self, customer_answer):
        return self.customer_persistence.process_customer_answer(customer_answer)


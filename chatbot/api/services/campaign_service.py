from chatbot.api.persistence.campaign_persistence import CampaignPersistence


class CampaignService:

    def __init__(self):
        """
            Constructor
        """
        self.customer_persistence = CampaignPersistence()

    def find_campaign_by_name(self, name):
        """
            Find a campaign by name.
        :return:
        """
        res = []
        all_campaigns = self.customer_persistence.find_campaign_by_name(name)
        for i in all_campaigns:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

    def find_campaign_by_description(self, description):
        """
            Find a campaign by description.
        :return:
        """
        res = []
        all_campaigns = self.customer_persistence.find_campaign_by_description(description)
        for i in all_campaigns:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

    def find_all_campaigns(self):
        """
            Find all the campaigns
        :return:
        """
        return self.customer_persistence.find_all_campaigns()

    def find_campaigns_by_status(self, state):
        """
            Find all the campaigns with state
        :param state:
        :return:
        """
        res = []
        if state == "N":
            res = self.customer_persistence.find_all_campaigns_deprecated()
        elif state == "A":
            res = self.customer_persistence.find_all_campaigns_active()
        return res

from chatbot.api.persistence.employee_persistence import EmployeePersistence


class EmployeeService:

    def __init__(self):
        """
            Constructor
        """
        self.employee_persistence = EmployeePersistence()

    def find_employee_by_name(self, name):
        """
            Find a employee by name.
        :return:
        """
        res = []
        all_employees= self.employee_persistence.find_employee_by_name(name)
        for i in all_employees:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

    def find_employee_by_area(self, name):
        """
            Find a employee by name.
        :return:
        """
        res = []
        all_employees = self.employee_persistence.find_employee_by_area(name)
        for i in all_employees:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

    def find_employee_by_helpers(self):
        """
            Find all employees that can help a customer.
        :return:
        """
        res = []
        all_employees = self.employee_persistence.find_employee_by_helpers()
        for i in all_employees:
            i["_id"] = str(i["_id"])
            res.append(i)
        return res

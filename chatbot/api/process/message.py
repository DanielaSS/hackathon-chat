from uuid import uuid4
import random

class Message:
    
    def __init__(self):
        self.message = []
        self.customer_state = 0
        self.token = ""
        
    def process_customer_offer(self,customer):
        self.message = []
        self.message.append("Hola " + customer["Nombres"] + ", buen día")
        if(customer["Estrellas"] == "1"):
            self.message.append("Desde el banco BBVA queremos recordarte el"\
            +" pago de tu producto: "+customer["Descripcion"])
            self.message.append("Puedes pagarlo a través de la aplicación BBVA")
            self.message.append("https://play.google.com/store/apps/details?id=co.com.bbva.mb")
            
        elif(customer["Estrellas"] == "2"):
            self._get_product_from_number(customer)

        elif(customer["Estrellas"] == "3"):
            self.message.append("Desde el banco BBVA queremos ofrecerte un sobregiro en tu"+
                                customer["Descripcion"])
            self.message.append("Te interesa?")
            self.message.append("1.Si")
            self.message.append("2.No")
        elif(customer["Estrellas"] == "4"):
            self.message.append("Desde el banco BBVA queremos ofrecerte la reestructuracion de")
            self.message.append(" tu " + customer["Descripcion"])
            self.message.append("Te interesa?")
            self.message.append("1.Si")
            self.message.append("2.No")
        elif(customer["Estrellas"] == "5"):
            self.message.append("Desde el banco BBVA queremos ofrecerte los siguientes productos: ")
            self.message.append("1.CDT")
            self.message.append("2.Cuenta de ahorros")
            self.message.append("3.Fondo de inversion")
            self.message.append("4.No me interesa")
        else:
            self.message.append("Desde el banco BBVA queremos ofrecerte los siguientes productos: ")
            self.message.append("1.CDT")
            self.message.append("2.Cuenta de ahorros")
            self.message.append("3.Fondo de inversion")
            self.message.append("4.Tarjeta de credito")
            self.message.append("5.Préstamo hipotecario")
            self.message.append("6.Libre inversion")
            self.message.append("7.No me interesa")


    def processing_customer_messages(self,customer,product):
        self.message = []
        if (product != "NO"):
            self._yes_no_product_customer("si", customer)
        else:
            self._information_message_customer()
            self.token = ""


    def _acceptance_bank_product(self,customer_email):
        self.message = []
        self.message.append("Muy bien, acambamos de enviarte un código de verificación")
        self.message.append("a tu correo " + customer_email.replace("\t",""))
        self.message.append("Por favor digitalo")
        self.token = self._generate_token()



    def contract_product_bank(self,customer_product):
        self.message = []
        self.message.append("Muchas gracias, tu solicitud para adquirir el producto"\
        +customer_product)
        self.message.append("ha sido aceptada")
        self.message.append("código de aceptación: "+self._random_number())
        self.message.append("Te contactaremos pronto, para seguir con el procedimiento.")


    def _information_message_customer(self):
        self.message = []
        self.message.append("Te puedo ayudar el algo más?")
        self.message.append("Escribe campañas")
        self.message.append("Escribe asesores")
        self.message.append("Escribe no, para salir.")


    def _yes_no_product_customer(self,customer_message,customer):
        if (customer_message.lower() == "si"):
            self._acceptance_bank_product(customer["Email"])
        elif (customer_message == "no"):
            self._information_message_customer()

    def _get_product_from_number(self,customer):
        self.message = []
        self.message.append("Desde el banco BBVA queremos")
        self.message.append("ofrecerte la renovación de tu préstamo: "+
                            customer["Descripcion"])
        self.message.append("Te interesa?")
        self.message.append("1.Si")
        self.message.append("2.No")

    def _random_number(self):
        return str(random.randint(100000,999999))

    def _generate_token(self):
        rand_token = uuid4()
        return str(rand_token)[0:7]

from chatbot.api.services.campaign_service import CampaignService
from django.http import HttpResponse, JsonResponse

campaign_service = CampaignService()


def get_all_campaign(request):
    if request.method == "GET":
        response = JsonResponse(campaign_service.find_all_campaigns(), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_campaign_by_status(request, state):
    if request.method == "GET":
        response = JsonResponse(campaign_service.find_campaigns_by_status(state), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_campaign_by_name(request, name):
    if request.method == "GET":
        response = JsonResponse(campaign_service.find_campaign_by_name(name), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_campaign_by_description(request, description):
    if request.method == "GET":
        response = JsonResponse(campaign_service.find_campaign_by_description(description), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

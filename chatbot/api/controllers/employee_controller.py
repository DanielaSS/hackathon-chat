from chatbot.api.services.employee_service import EmployeeService
from django.http import HttpResponse, JsonResponse

employee_service = EmployeeService()


def get_employee_by_name(request, name):
    if request.method == "GET":
        response = JsonResponse(employee_service.find_employee_by_name(name), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_employee_by_area(request, area):
    if request.method == "GET":
        response = JsonResponse(employee_service.find_employee_by_area(area), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_employee_by_helpers(request):
    if request.method == "GET":
        response = JsonResponse(employee_service.find_employee_by_helpers(), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

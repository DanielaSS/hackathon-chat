from chatbot.api.services.customer_service import CustomerService
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
import json

customer_service = CustomerService()


def _convert_cursor_to_json(cursor):
    """Returns the cursor in a str format.

    :param cursor: A bson
    :return: array, a string with the cursor information.
    """
    res = []
    for i in cursor:
        res.append(i)
    return res


def get_customer_by_phone(request,phone):
    if request.method == "GET":
        response = JsonResponse(customer_service.get_customer_by_phone(phone), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def get_process_customer_answer(request, phone):
    if request.method == "POST":
        response = JsonResponse(customer_service.process_customer_answer(json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

from django.urls import path
from chatbot.api.controllers import customer_controller, campaign_controller, employee_controller


urlpatterns = [
    path('customer/<phone>/', customer_controller.get_customer_by_phone),
    path('customer/<phone>/answer/', customer_controller.get_process_customer_answer),

    path('campaigns/', campaign_controller.get_all_campaign),
    path('campaigns/name=<name>/', campaign_controller.get_campaign_by_name),
    path('campaigns/description=<description>/', campaign_controller.get_campaign_by_description),
    path('campaigns/status=<state>/', campaign_controller.get_campaign_by_status),

    path('employees/name=<name>/', employee_controller.get_employee_by_name),
    path('employees/area=<area>/', employee_controller.get_employee_by_area),
    path('employees/helpers/', employee_controller.get_employee_by_helpers),
]


import os

from django.core.wsgi import get_wsgi_application

#Always have to be in development
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'chatbot.settings.development')

application = get_wsgi_application()
